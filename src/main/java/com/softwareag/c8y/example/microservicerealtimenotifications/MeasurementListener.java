package com.softwareag.c8y.example.microservicerealtimenotifications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.cumulocity.microservice.context.ContextService;
import com.cumulocity.microservice.context.ContextServiceImpl;
import com.cumulocity.microservice.context.credentials.MicroserviceCredentials;
import com.cumulocity.microservice.subscription.model.MicroserviceSubscriptionAddedEvent;
import com.cumulocity.sdk.client.Platform;
import com.cumulocity.sdk.client.PlatformParameters;
import com.cumulocity.sdk.client.measurement.MeasurementApi;
import com.cumulocity.sdk.client.measurement.MeasurementCollection;
import com.cumulocity.sdk.client.measurement.PagedMeasurementCollectionRepresentation;
import com.cumulocity.sdk.client.notification.Subscriber;
import com.cumulocity.sdk.client.notification.SubscriberBuilder;
import com.cumulocity.sdk.client.notification.Subscription;
import com.cumulocity.sdk.client.notification.SubscriptionListener;
import com.cumulocity.sdk.client.notification.SubscriptionNameResolver;

/**
 * Illustrates waiting for the service to bootstrap itself and then
 * 
 * 1) using the measurement api to retrieve some measurements
 * 
 * 2) subscribing to real time measurement notifications
 * 
 * For real-time notification docs see
 * https://www.cumulocity.com/guides/reference/real-time-notifications/
 * 
 * @author MKOS
 *
 */
@Component
public class MeasurementListener {

	@Autowired
	private Platform tenantPlatform;

	/**
	 * For other c8y APIs and components that can be injected with the
	 * 
	 * @Autowired annotation, see
	 *            {@link com.cumulocity.microservice.api.CumulocityClientFeature.java}
	 */
	@Autowired
	private MeasurementApi measurementApi;

	@Autowired
	private ContextService<MicroserviceCredentials> microserviceContextService;

	public MeasurementListener() {
	}

	@EventListener
	public void handleEvent(Object event) {
		/*
		 * This event is fired after we have used out bootstrap credentials to get our
		 * service credentials(the credentials we use to talk to the tenant). It is
		 * fired once for each tenant that is subscribed to this microservice
		 */
		if (event instanceof MicroserviceSubscriptionAddedEvent) {
			MicroserviceSubscriptionAddedEvent subscriptionAddedEvent = (MicroserviceSubscriptionAddedEvent) event;
			MicroserviceCredentials credentials = subscriptionAddedEvent.getCredentials();
			System.out.println("credentials: " + credentials);

			setupNotifications();

			/*
			 * We're calling methods of the measurementApi here. Note that to be able to do
			 * this, generally we need to wrap the call in
			 * ContextService.callWithinContext(), which provides the credentials to make
			 * platform API calls. Here we don't need to, because further up the call stack,
			 * the code that called our handleEevent method has called
			 * ContextServiceImpl.callWithinContext() for us
			 */
			System.out.println("Calling retrieveMeasurements from within a MicroserviceSubscriptionAddedEvent");
			retrieveMeasurements();

			/*
			 * But in a new Thread, retrieveMeasurements will fail complaining that
			 * "Scope 'tenant' is not active for the current thread":
			 */
			new Thread(() -> {
				try {
					System.out.println("Calling retrieveMeasurements outside a context..");
					retrieveMeasurements();
				} catch (Exception e) {
					// exception is java.lang.IllegalStateException: Not within any context! }
					System.out.println("expected exception (Not within any context! )" + e);
				}
			}).start();

			// Wrapping the call in context should work:
			new Thread(() -> {
				System.out.println("Calling retrieveMeasurements within context..");
				microserviceContextService.runWithinContext(credentials, () -> {
					retrieveMeasurements();
				});
			}).start();
		}
		System.out.println("event: " + event);
	}

	private void retrieveMeasurements() {
		MeasurementCollection measurementCollection = measurementApi.getMeasurements();
		PagedMeasurementCollectionRepresentation measurementPage = measurementCollection.get(10);
		System.out.println("A measurement: " + measurementPage.getMeasurements().get(0));
	}

	private void setupNotifications() {
		String channelId = "/measurements/*";

		/*
		 * SubscriberBuilder<T, R> creates a Subscriber<T, R>, which can be used to
		 * subscribe to real-time notifications (using Bayeux long polling).
		 * 
		 * R is the message type; when we receive a notification message, it will be
		 * converted to type R.
		 * 
		 * T - when we subscribe via subscriber.subscribe, T will be used by the
		 * SubscriptionNameResolver<T> to generate the Bayeux subscription channel name,
		 * e.g. if T is of type Device, a SubscriptionNameResolver<Device> may return
		 * "/alarms/<deviceId>". Here we have a trivial case where T is a string
		 * containing the channel name and the SubscriptionNameResolver returns it
		 * unchanged.
		 * 
		 * Other endpoints available: cep/realtime for inventory, events, measurements
		 * and alarms devicecontrol/notifications for device control cep/notifications
		 * for real-time statements (output from CEP)
		 * 
		 * 
		 */
		Subscriber<String, Object> subscriber = SubscriberBuilder.<String, Object>anSubscriber()
				.withParameters((PlatformParameters) tenantPlatform).withEndpoint("cep/realtime")
				.withSubscriptionNameResolver(new Identity()).withDataType(Object.class).build();

		SubscriptionListener<String, Object> handler = new SubscriptionListener<String, Object>() {
			@Override
			public void onNotification(Subscription<String> subscription, Object notification) {
				System.out.println("Received notification: " + notification);
			}

			@Override
			public void onError(Subscription<String> subscription, Throwable ex) {
				System.out.println("Subscription error: " + ex);
			}
		};

		System.out.println("Subscribing to channel: " + channelId);
		Subscription<String> subscription = subscriber.subscribe(channelId, handler);
	}

	private static final class Identity implements SubscriptionNameResolver<String> {
		@Override
		public String apply(String id) {
			return id;
		}
	}

}
